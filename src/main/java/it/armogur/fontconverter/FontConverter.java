package it.armogur.fontconverter;

import processing.core.PApplet;
import processing.core.PFont;
import processing.opengl.PGraphics2D;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * @author : istvan.horvath
 */
public class FontConverter {
	static {
		System.setProperty("java.awt.headless", "true");
		// PApplet static init dirty nasty fucking hack
		System.setProperty("java.version", System.getProperty("java.version") + ".0");
	}

	public static void main(String[] args) {
		int count = 0;
		int interval;

		for (int i = 0; i < unicodeBlocks.length; i += 2) {
			interval = unicodeBlocks[i + 1] - unicodeBlocks[i];
			if (interval < 0) {
				throw new RuntimeException("ERROR: Bad Unicode range specified, last < first!");
			}
			count += interval + 1;
		}
		String additionalLetters;
		// middle bar font: additionalLetters = "-";
		// datebar font: additionalLetters = ".:%" + (char) 0x2103 + (char) 0x3371;
		additionalLetters = ".:%" + (char) 0x2103 + (char) 0x3371;
		count += additionalLetters.length();

		System.out.println("Total number of characters  = " + count);

		if (count == 0) {
			throw new RuntimeException("ERROR: No Unicode range or specific codes have been defined!");
		}

		char[] charset = new char[count];
		int index = 0;

		for (int i = 0; i < unicodeBlocks.length; i += 2) {
			// loading the range specified
			for (int code = unicodeBlocks[i]; code <= unicodeBlocks[i + 1]; code++) {
				charset[index] = Character.toChars(code)[0];
				index++;
			}
		}

		// loading the specific point codes
		for (int i = 0; i < additionalLetters.length(); i++) {
			charset[index++] = additionalLetters.charAt(i);
		}
		System.out.println("charset = " + Arrays.toString(charset));
		String inputFontName = "ArialUnicodeMS";

		saveToFile(inputFontName, "date-font.vlw", 51, charset);
		saveToHeaderFile(inputFontName, "date-font.h", "DateFont", 51, charset);
	}

	private static void saveToFile(String inputFontName, String outputFontName, int fontSize, char[] charset) {
		System.out.println("Creating font file: " + outputFontName);
		try (OutputStream output = new BufferedOutputStream(new FileOutputStream(outputFontName))) {
			PFont font = new MyGraphics().createFont(inputFontName, fontSize, true, charset);

			font.save(output);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static void saveToHeaderFile(String inputFontName, String outputFontName, String variableName,
			int fontSize, char[] charset) {
		System.out.println("Creating font file: " + outputFontName);
		try (ByteArrayOutputStream output = new ByteArrayOutputStream();
				BufferedWriter headerFile = new BufferedWriter(new FileWriter(outputFontName))) {
			PFont font = new MyGraphics().createFont(inputFontName, fontSize, true, charset);

			font.save(output);

			headerFile.write("#include <pgmspace.h>\n\nconst uint8_t ");
			headerFile.write(variableName);
			headerFile.write("[] PROGMEM = {\n");

			byte[] bytes = output.toByteArray();
			int rowBytesWritten = 0;
			for (int i = 0; i < bytes.length; i++) {
				if (i % 16 == 0) {
					headerFile.write("        ");
				}
				headerFile.write("0x");
				headerFile.write(String.format("%02x", bytes[i] & 0xff));
				rowBytesWritten++;
				if (i != bytes.length - 1) {
					if (rowBytesWritten < 16) {
						headerFile.write(", ");
					} else {
						headerFile.write(",");
					}
				}
				if (rowBytesWritten == 16) {
					headerFile.newLine();
					rowBytesWritten = 0;
				}
			}
			headerFile.newLine();
			headerFile.write("};\n");

			System.out.println(outputFontName + " written.");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static class MyGraphics extends PGraphics2D {

		public MyGraphics() {
			super();
			PApplet pApplet = new PApplet();
			pApplet.sketchPath();
			super.setParent(pApplet);
		}

		@Override
		protected PFont createFont(String name, float size, boolean smooth, char[] charset) {
			return super.createFont(name, size, smooth, charset);
		}
	}

	static final int[] unicodeBlocks = {
			0x0030, 0x0039, //Example custom range (numbers 0-9)
//			0x0061, 0x007A, //Example custom range (Lower case a-z)
//			0x0041, 0x005A, //Example custom range (Upper case A-Z)
	};
}
