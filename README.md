# TTF to VLW converter

TTF to VLW converter project for [TFT_eSPI](https://github.com/Bodmer/TFT_eSPI).  
This designed to work without the [Processing](https://processing.org/) editor, you can use this in your favourite IDE or even without it on command line.  
Output can be .vlw or .h file as well.  
Tested in MacOS, this should work under Linux as well,
maybe you should tune the **download-jogl.sh** shell script.

## Getting started

Install JDK 11 and gradle 7.1 and maven.  
Adjust these variables in FontConverter.java:
```
unicodeBlocks
additionalLetters
inputFontName
```
Then call one of these two methods: ```saveToFile``` or ```saveToHeaderFile``` with ```outputFontName``` and ```fontSize``` parameter adjusted to your need.

## Compilation

At first stage gradle will download unreleased version of Java OpenGL 2.4.0 RC which can not be found in  
mvn repositories and will create a local one in src/resources/repo

## Reference
This work is based on: [Create_font.pde](https://github.com/Bodmer/TFT_eSPI/blob/master/Tools/Create_Smooth_Font/Create_font/Create_font.pde)
