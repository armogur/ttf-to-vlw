fingerprint_examiner() {
  echo "Checking fingerprint of $1 "
  desired_fingerprint="eaeb75adf72c801ee840ba9c868f3fa5"
  if [ -f "$1" ]; then
    fingerprint=$(md5 -q "$1")
    if [ "$fingerprint" != $desired_fingerprint ]; then
      echo "mismatch $fingerprint != $desired_fingerprint";
      exit 1
    else
      echo "Okay."
    fi
  else
    echo "$1 not found!"
    exit 1
  fi
}

repo_jar_fqdn="src/main/resources/repo/org/jogamp/jogl-fat/2.4.0-rc-20210111/jogl-fat-2.4.0-rc-20210111.jar"

if [ ! -f $repo_jar_fqdn ]; then
  if [ ! -f jogamp-fat.jar ]; then
    echo "Downloading jogamp-fat.jar ... "
    if ! wget https://jogamp.org/deployment/archive/rc/v2.4.0-rc-20210111/fat/jogamp-fat.jar ; then
      echo "Failed to download!"
      exit 1
    else
      echo "Done."
    fi
  fi

  fingerprint_examiner "jogamp-fat.jar"

  mvn install:install-file -Dfile=jogamp-fat.jar \
    -DgroupId=org.jogamp \
    -DartifactId=jogl-fat \
    -Dversion=2.4.0-rc-20210111 \
    -Dpackaging=jar -DlocalRepositoryPath=src/main/resources/repo/
fi

fingerprint_examiner $repo_jar_fqdn
